﻿// See https://aka.ms/new-console-template for more information

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace FileCounter
{
    internal class Program
    {
        private static void Main()
        {
            //dirName путь для скана
            string dirName = @"C:\test";
            
            Dictionary<int, StringBuilder> listFolder = new Dictionary<int, StringBuilder>();
            string[] dirs2;
            
            //чтение папок
            string[] dirs = Directory.GetDirectories(dirName);
            //Поиск папок с N количеством файлов
            foreach (var folder in dirs)
            {
                dirs2 = Directory.GetFiles(folder);
                if (listFolder.ContainsKey(dirs2.Length))
                    listFolder[dirs2.Length].Append($@",{ConvName(folder)}");
                else
                {
                    listFolder.Add(dirs2.Length, new StringBuilder(ConvName(folder)));
                }
            }

            string consoleComand =
                @"C:\zabbix\win64\zabbix_sender.exe -z 192.168.32.107 -p 10051 -I 192.168.21.97 -s MPVS-JOBSERVER";
            /*
             * после ключа -к ввести нужное значение
             */

            if (!listFolder.ContainsKey(2))
            {
                Process.Start("cmd.exe", "/C" + @$"{consoleComand} -k ResultFolder2 -o 0");
            }
            if (!listFolder.ContainsKey(3))
            {
                Process.Start("cmd.exe", "/C" + @$"{consoleComand} -k ResultFolder3 -o 0");
            }
            
            foreach (var folder in listFolder)
            {
                //Console.WriteLine(@$" {consoleComand} -k ResultFolder{folder.Key} -o {folder.Value}");
                Process.Start("cmd.exe", "/C" + @$"{consoleComand} -k ResultFolder{folder.Key} -o {folder.Value}");
            }
        }

        private static string ConvName(string path)
        {
            DirectoryInfo dirInfo = new(path);
            return dirInfo.Name;
        }
    }
}

